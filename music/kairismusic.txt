# Songs of War
# Five songs to play during war
# For various Halessi stuff

song = {
	name = "Emperors_Road"
	
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { religion_group = halessi }
			NOT = { any_owned_province = { continent = asia } }
		}
		modifier = {
			factor = 2
			religion_group = halessi
		}
		modifier = {
			factor = 2
			culture_group = hobgoblin
		}
	}
}

song = {
	name = "Forest_Shade"
	
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { religion_group = halessi }
			NOT = { any_owned_province = { continent = asia } }
		}
		modifier = {
			factor = 2
			religion_group = halessi
		}
		modifier = {
			factor = 2
			culture_group = hobgoblin
		}
	}
}

song = {
	name = "Jade_Ambitions"
	
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { religion_group = halessi }
			NOT = { any_owned_province = { continent = asia } }
		}
		modifier = {
			factor = 2
			religion_group = halessi
		}
		modifier = {
			factor = 2
			culture_group = hobgoblin
		}
	}
}

song = {
	name = "Silken_Path"
	
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { religion_group = halessi }
			NOT = { any_owned_province = { continent = asia } }
		}
		modifier = {
			factor = 2
			religion_group = halessi
		}
		modifier = {
			factor = 2
			culture_group = hobgoblin
		}
	}
}

song = {
	name = "Takeda_Sunrise"
	
	chance = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { religion_group = halessi }
			NOT = { any_owned_province = { continent = asia } }
		}
		modifier = {
			factor = 2
			religion_group = halessi
		}
		modifier = {
			factor = 2
			culture_group = hobgoblin
		}
	}
}

